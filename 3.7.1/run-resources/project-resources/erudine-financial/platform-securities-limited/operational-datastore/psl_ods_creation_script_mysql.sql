--
-- Create and use database `psl_ods`
--
DROP DATABASE IF EXITS `psl_ods`;
CREATE DATABASE `psl_ods`;
USE `psl_ods`;

--
-- Table structure for table `acctrans`
--

DROP TABLE IF EXISTS `acctrans`;
CREATE TABLE `acctrans` (
  `ACCTRANS_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACCTRANS_UID`)
);

-- Table structure for table `acctrans_contract`
--

DROP TABLE IF EXISTS `acctrans_contract`;
CREATE TABLE `acctrans_contract` (
  `ACCTRANS_Contract_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACCTRANS_ContractNotes_UID` bigint(20) NOT NULL,
  `BargainDate` varchar(255) DEFAULT NULL,
  `BargainTime` varchar(255) DEFAULT NULL,
  `BuyOrSell` varchar(255) DEFAULT NULL,
  `ClientConsideration` varchar(255) DEFAULT NULL,
  `Consideration` varchar(255) DEFAULT NULL,
  `ConsiderationCurrency` varchar(255) DEFAULT NULL,
  `DealingCharge` varchar(255) DEFAULT NULL,
  `ExchangeRate` varchar(255) DEFAULT NULL,
  `ExtraCharges` varchar(255) DEFAULT NULL,
  `Interest` varchar(255) DEFAULT NULL,
  `OrderType` varchar(255) DEFAULT NULL,
  `PTM` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `Quantity` varchar(255) DEFAULT NULL,
  `R1` varchar(255) DEFAULT NULL,
  `R2` varchar(255) DEFAULT NULL,
  `SettlementCurrency` varchar(255) DEFAULT NULL,
  `Stamp` varchar(255) DEFAULT NULL,
  `TotalClientCharges` varchar(255) DEFAULT NULL,
  `Venue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ACCTRANS_Contract_UID`)
);

-- Table structure for table `acctrans_contractnotes`
--

DROP TABLE IF EXISTS `acctrans_contractnotes`;
CREATE TABLE `acctrans_contractnotes` (
  `ACCTRANS_ContractNotes_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACCTRANS_UID` bigint(20) NOT NULL,
  PRIMARY KEY (`ACCTRANS_ContractNotes_UID`)
);

--
-- Table structure for table `corpevents`
--

DROP TABLE IF EXISTS `corpevents`;
CREATE TABLE `corpevents` (
  `CORPEVENTS_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CORPEVENTS_UID`)
);

--
-- Table structure for table `corpevents_corporateaction`
--

DROP TABLE IF EXISTS `corpevents_corporateaction`;
CREATE TABLE `corpevents_corporateaction` (
  `CORPEVENTS_CorporateAction_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CORPEVENTS_UID` bigint(20) NOT NULL,
  `EXDate` varchar(255) DEFAULT NULL,
  `EventTypeCode` varchar(255) DEFAULT NULL,
  `OldStockR1` varchar(255) DEFAULT NULL,
  `OldStockR2` varchar(255) DEFAULT NULL,
  `PaymentDate` varchar(255) DEFAULT NULL,
  `RatioNew` varchar(255) DEFAULT NULL,
  `RatioOld` varchar(255) DEFAULT NULL,
  `StockR1` varchar(255) DEFAULT NULL,
  `StockR2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CORPEVENTS_CorporateAction_UID`)
);

--
-- Table structure for table `feespaid`
--

DROP TABLE IF EXISTS `feespaid`;
CREATE TABLE `feespaid` (
  `FEESPAID_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) NOT NULL,
  PRIMARY KEY (`FEESPAID_UID`)
);

--
-- Table structure for table `feespaid_feeline`
--

DROP TABLE IF EXISTS `feespaid_feeline`;
CREATE TABLE `feespaid_feeline` (
  `FEESPAID_FeeLine_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FEESPAID_UID` bigint(20) NOT NULL,
  `FeeClient` varchar(255) DEFAULT NULL,
  `FeeCurrency` varchar(255) DEFAULT NULL,
  `FeeValue` varchar(255) DEFAULT NULL,
  `TotalAdvisoryFees` varchar(255) DEFAULT NULL,
  `TotalOtherFees` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FEESPAID_FeeLine_UID`)
);

--
-- Table structure for table `psrdata`
--

DROP TABLE IF EXISTS `psrdata`;
CREATE TABLE `psrdata` (
  `PSRDATA_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) NOT NULL,
  PRIMARY KEY (`PSRDATA_UID`)
);

--
-- Table structure for table `psrdata_clientrecord`
--

DROP TABLE IF EXISTS `psrdata_clientrecord`;
CREATE TABLE `psrdata_clientrecord` (
  `PSRDATA_ClientRecord_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PSRDATA_UID` bigint(20) NOT NULL,
  `PeriodEnd` varchar(255) DEFAULT NULL,
  `ClientReturn` varchar(255) DEFAULT NULL,
  `PeriodStart` varchar(255) DEFAULT NULL,
  `EndDate3Months` varchar(255) DEFAULT NULL,
  `ClientReturn3Months` varchar(255) DEFAULT NULL,
  `EndDate6Months` varchar(255) DEFAULT NULL,
  `ClientReturn6Months` varchar(255) DEFAULT NULL,
  `EndDate12Months` varchar(255) DEFAULT NULL,
  `ClientReturn12Months` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PSRDATA_ClientRecord_UID`)
);

--
-- Table structure for table `psrdata_kycdetails`
--

DROP TABLE IF EXISTS `psrdata_kycdetails`;
CREATE TABLE `psrdata_kycdetails` (
  `PSRDATA_KycDetails_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PSRDATA_UID` bigint(20) NOT NULL,
  `RiskProfile` varchar(255) DEFAULT NULL,
  `Objective` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PSRDATA_KycDetails_UID`)
);

--
-- Table structure for table `psrdata_nameandaddress`
--

DROP TABLE IF EXISTS `psrdata_nameandaddress`;
CREATE TABLE `psrdata_nameandaddress` (
  `PSRDATA_NameAndAddress_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PSRDATA_UID` bigint(20) NOT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `ClientName` varchar(255) DEFAULT NULL,
  `Product` varchar(255) DEFAULT NULL,
  `InvestmentObjectives` varchar(255) DEFAULT NULL,
  `ExtraAddress2` varchar(255) DEFAULT NULL,
  `MainTsaCategory` varchar(255) DEFAULT NULL,
  `CategoryTypeDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PSRDATA_NameAndAddress_UID`)
);

--
-- Table structure for table `psrdata_portfolioindex`
--

DROP TABLE IF EXISTS `psrdata_portfolioindex`;
CREATE TABLE `psrdata_portfolioindex` (
  `PSRDATA_PortfolioIndex_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PSRDATA_UID` bigint(20) NOT NULL,
  `BenchMarkSedol` varchar(255) DEFAULT NULL,
  `BenchMarkDescription2` varchar(255) DEFAULT NULL,
  `TotalReturnPercent` varchar(255) DEFAULT NULL,
  `ReturnTypeFlag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PSRDATA_PortfolioIndex_UID`)
);

--
-- Table structure for table `psrdata_summaryrecord`
--

DROP TABLE IF EXISTS `psrdata_summaryrecord`;
CREATE TABLE `psrdata_summaryrecord` (
  `PSRDATA_SummaryRecord_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PSRDATA_UID` bigint(20) NOT NULL,
  `PeriodEndValue` varchar(255) DEFAULT NULL,
  `MovementsInInPeriod` varchar(255) DEFAULT NULL,
  `MovementsOutInPeriod` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PSRDATA_SummaryRecord_UID`)
);

--
-- Table structure for table `routing`
--

DROP TABLE IF EXISTS `routing`;
CREATE TABLE `routing` (
  `ROUTING_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PersonCode` varchar(255) DEFAULT NULL,
  `SystemCode` varchar(255) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `AdHocPrint` varchar(255) DEFAULT NULL,
  `PackNumber` varchar(255) DEFAULT NULL,
  `PrintEndDate` varchar(255) DEFAULT NULL,
  `PrintStartDate` varchar(255) DEFAULT NULL,
  `ValuationCurrency` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Address1` varchar(255) DEFAULT NULL,
  `Address2` varchar(255) DEFAULT NULL,
  `Address3` varchar(255) DEFAULT NULL,
  `Address4` varchar(255) DEFAULT NULL,
  `Address5` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `ClientTypeDescription` varchar(255) DEFAULT NULL,
  `MainTsaCategory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ROUTING_UID`)
);

--
-- Table structure for table `statepcap`
--

DROP TABLE IF EXISTS `statepcap`;
CREATE TABLE `statepcap` (
  `STATEPCAP_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPCAP_UID`)
);

--
-- Table structure for table `statepcap_statement`
--

DROP TABLE IF EXISTS `statepcap_statement`;
CREATE TABLE `statepcap_statement` (
  `STATEPCAP_Statement_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPCAP_UID` bigint(20) NOT NULL,
  `BfBalance` varchar(255) DEFAULT NULL,
  `ClientNumber` varchar(255) DEFAULT NULL,
  `Currency` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPCAP_Statement_UID`)
);

--
-- Table structure for table `statepcap_statementitem`
--

DROP TABLE IF EXISTS `statepcap_statementitem`;
CREATE TABLE `statepcap_statementitem` (
  `STATEPCAP_StatementItem_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPCAP_Statement_UID` bigint(20) NOT NULL,
  `Balance` varchar(255) DEFAULT NULL,
  `BatchType` varchar(255) DEFAULT NULL,
  `CreditAmount` varchar(255) DEFAULT NULL,
  `DebitAmount` varchar(255) DEFAULT NULL,
  `Ledger` varchar(255) DEFAULT NULL,
  `Description1` varchar(255) DEFAULT NULL,
  `Description2` varchar(255) DEFAULT NULL,
  `Description3` varchar(255) DEFAULT NULL,
  `Description4` varchar(255) DEFAULT NULL,
  `Description5` varchar(255) DEFAULT NULL,
  `Description6` varchar(255) DEFAULT NULL,
  `StatementDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPCAP_StatementItem_UID`)
);

--
-- Table structure for table `statepcap_statementtotal`
--

DROP TABLE IF EXISTS `statepcap_statementtotal`;
CREATE TABLE `statepcap_statementtotal` (
  `STATEPCAP_StatementTotal_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPCAP_Statement_UID` bigint(20) NOT NULL,
  `CfBalance` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPCAP_StatementTotal_UID`)
);

--
-- Table structure for table `statepinc`
--

DROP TABLE IF EXISTS `statepinc`;
CREATE TABLE `statepinc` (
  `STATEPINC_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) NOT NULL,
  PRIMARY KEY (`STATEPINC_UID`)
);

--
-- Table structure for table `statepinc_statement`
--

DROP TABLE IF EXISTS `statepinc_statement`;
CREATE TABLE `statepinc_statement` (
  `STATEPINC_Statement_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPINC_UID` bigint(20) NOT NULL,
  `BfBalance` varchar(255) DEFAULT NULL,
  `ClientNumber` varchar(255) DEFAULT NULL,
  `Currency` varchar(255) DEFAULT NULL,
  `EndDate` varchar(255) DEFAULT NULL,
  `StartDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPINC_Statement_UID`)
);

--
-- Table structure for table `statepinc_statementitem`
--

DROP TABLE IF EXISTS `statepinc_statementitem`;
CREATE TABLE `statepinc_statementitem` (
  `STATEPINC_StatementItem_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPINC_Statement_UID` bigint(20) NOT NULL,
  `Balance` varchar(255) DEFAULT NULL,
  `BatchType` varchar(255) DEFAULT NULL,
  `CreditAmount` varchar(255) DEFAULT NULL,
  `DebitAmount` varchar(255) DEFAULT NULL,
  `Ledger` varchar(255) DEFAULT NULL,
  `Description1` varchar(255) DEFAULT NULL,
  `Description2` varchar(255) DEFAULT NULL,
  `Description3` varchar(255) DEFAULT NULL,
  `Description4` varchar(255) DEFAULT NULL,
  `Description5` varchar(255) DEFAULT NULL,
  `Description6` varchar(255) DEFAULT NULL,
  `StatementDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPINC_StatementItem_UID`)
);

--
-- Table structure for table `statepinc_statementtotal`
--

DROP TABLE IF EXISTS `statepinc_statementtotal`;
CREATE TABLE `statepinc_statementtotal` (
  `STATEPINC_StatementTotal_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATEPINC_Statement_UID` bigint(20) NOT NULL,
  `CfBalance` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`STATEPINC_StatementTotal_UID`)
);

--
-- Table structure for table `valuation`
--

DROP TABLE IF EXISTS `valuation`;
CREATE TABLE `valuation` (
  `VALUATION_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PrintCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_UID`)
);

--
-- Table structure for table `valuation_cashline`
--

DROP TABLE IF EXISTS `valuation_cashline`;
CREATE TABLE `valuation_cashline` (
  `VALUATION_CashLine_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VALUATION_UID` bigint(20) NOT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `BalanceInValCur` varchar(255) DEFAULT NULL,
  `CashBalance` varchar(255) DEFAULT NULL,
  `CashCurrency` varchar(255) DEFAULT NULL,
  `ExchangeRate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_CashLine_UID`)
);

--
-- Table structure for table `valuation_sector1`
--

DROP TABLE IF EXISTS `valuation_sector1`;
CREATE TABLE `valuation_sector1` (
  `VALUATION_Sector1_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VALUATION_UID` bigint(20) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_Sector1_UID`)
);

--
-- Table structure for table `valuation_sector1total`
--

DROP TABLE IF EXISTS `valuation_sector1total`;
CREATE TABLE `valuation_sector1total` (
  `VALUATION_Sector1Total_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VALUATION_Sector1_UID` bigint(20) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_Sector1Total_UID`)
)

--
-- Table structure for table `valuation_valdetail`
--

DROP TABLE IF EXISTS `valuation_valdetail`;
CREATE TABLE `valuation_valdetail` (
  `VALUATION_ValDetail_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VALUATION_Sector1_UID` bigint(20) NOT NULL,
  `Holding` varchar(255) DEFAULT NULL,
  `StockIssuer` varchar(255) DEFAULT NULL,
  `SecurityDescription` varchar(255) DEFAULT NULL,
  `Cost` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `Income` varchar(255) DEFAULT NULL,
  `Yield` varchar(255) DEFAULT NULL,
  `AccruedInterest` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  `MarketCurrency` varchar(255) DEFAULT NULL,
  `AccruedInterestDays` varchar(255) DEFAULT NULL,
  `InNomineesName` varchar(255) DEFAULT NULL,
  `InFirmsControl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_ValDetail_UID`)
);

--
-- Table structure for table `valuation_valtotal`
--

DROP TABLE IF EXISTS `valuation_valtotal`;
CREATE TABLE `valuation_valtotal` (
  `VALUATION_ValTotal_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VALUATION_UID` bigint(20) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Income` varchar(255) DEFAULT NULL,
  `Yield` varchar(255) DEFAULT NULL,
  `AccountNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VALUATION_ValTotal_UID`)
);

--
-- Table structure for table `valuation_valuation`
--

DROP TABLE IF EXISTS `valuation_valuation`;
CREATE TABLE `valuation_valuation` (
  `VALUATION_Valuation_UID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INSERTION_AUDIT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Sector1` varchar(255) NOT NULL,
  PRIMARY KEY (`VALUATION_Valuation_UID`)
);